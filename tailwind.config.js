module.exports = {
    mode: 'jit',
    purge: {
        content: [
            './resources/**/*.antlers.html',
            './resources/**/*.blade.php',
            './content/**/*.md'
        ]
    },
    important: true,
    theme: {
        extend: {},
    },
    variants: {
        extend: {

            rotate: ['group-hover', 'focus-within', 'focus'],
            transform: ['group-hover', 'focus-within', 'focus'],
            translate: ['group-hover', 'focus-within', 'focus'],
            transitionDuration: ['group-hover'],
            transitionProperty: ['group-hover'],
            transitionTimingFunction: ['group-hover'],
            margin: ['group-hover'],
            fill: ['group-hover'],
            display: ['group-hover'],
            height: ['responsive', 'hover', 'focus']

        },
    },
    plugins: [],
}
