Ulster Scots Agency
        
        

Up and Running

• PHP - >v7.3            
• Node - v16  
• Valet - valet link  
• Browser Sync - npm run watch



Locations

• CSS - tailwind.css            
• JS - site.js            



Included

• Statamic Pro - ✓            
• Mailtrap - ✓    



Tailwind Extends
              
• rotate: ['group-hover', 'focus-within', 'focus'],                        
•  transform: ['group-hover', 'focus-within', 'focus'],                        
•  translate: ['group-hover', 'focus-within', 'focus'],                         
•  transitionDuration: ['group-hover'],                         
•  transitionProperty: ['group-hover'],                         
•  transitionTimingFunction: ['group-hover'],                         
•  margin: ['group-hover'],                         
•  fill: ['group-hover'],                         
•  display: ['group-hover'],                         
•  height: ['responsive', 'hover', 'focus']                     
                

                        
User Login Details
                
• Email -  hello@reflex-studios.com                    
• Super User -  ✓                    
• Password - π x 6432123 / 0.3456
• Wee jokes, it's 123456789


.env

APP_NAME=Statamic
APP_ENV=local
APP_KEY=base64:psDjM4+3xGYV3WITy1hg90+2h9VxweRRp9lqW4UFLqc=
APP_DEBUG=true
APP_URL=http://ulster-scots-agency.test

STATAMIC_LICENSE_KEY=
STATAMIC_STACHE_WATCHER=true
STATAMIC_STATIC_CACHING_STRATEGY=null
STATAMIC_REVISIONS_ENABLED=false
STATAMIC_GRAPHQL_ENABLED=false
STATAMIC_API_ENABLED=false
STATAMIC_GIT_ENABLED=false

LOG_CHANNEL=stack

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=ulster_scots_agency
DB_USERNAME=root
DB_PASSWORD=

BROADCAST_DRIVER=log
CACHE_DRIVER=file
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_MAILER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=b7aed64ffaa939
MAIL_PASSWORD=7d3564b6407cf4
MAIL_ENCRYPTION=tls
MAIL_FROM_ADDRESS=sam@reflex-studios.com
MAIL_FROM_NAME="${APP_NAME}"

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1

AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"

DEBUGBAR_ENABLED=false
